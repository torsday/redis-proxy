package main

import (
    "fmt"
    "io/ioutil"
    "net"
    "net/http"
    "testing"
    "time"

    "github.com/go-redis/redis"
    "github.com/stretchr/testify/assert"

    "red-prox/proxy"
)

var (
    testRedis = "redis:6379"
    testCapacity = 10
    testExpiration = 100
    testPort = 8080
    testMaxClients = 10
)

func getRedisClient(t *testing.T) *redis.Client {
    return redis.NewClient(&redis.Options{
        Addr: testRedis,
    })
}

func getRequestBody(path string, s *http.Server, t *testing.T) string {
    res := getResponse(path, s, t)

    body, _ := ioutil.ReadAll(res.Body)
    _ = res.Body.Close()

    return string(body)
}

func getResponse(path string, s *http.Server, t *testing.T) *http.Response {
    // Pick port automatically for parallel tests and to avoid conflicts
    l, _ := net.Listen("tcp", ":0")

    defer l.Close()
    go s.Serve(l) 
    
    res, _ := http.Get("http://" + l.Addr().String() + path)

    return res
}

func TestRequestKeyFromProxy(t *testing.T) {
    p := proxy.New(testRedis, testCapacity, testExpiration, testMaxClients)
    s := proxy.NewServer(testPort, p)
    client := getRedisClient(t)

    client.Set("test_key", "test_val", 0)

    response := getRequestBody("/keys/test_key", s, t)

    assert.Equal(t, "test_val", string(response), "proxy didn't find correct val")
}

func TestRequestKeyFromLRUCache(t *testing.T) {
    p := proxy.New(testRedis, testCapacity, testExpiration, testMaxClients)
    s := proxy.NewServer(testPort, p)
    client := getRedisClient(t)

    client.Set("test_key", "test_val", 0)

    getRequestBody("/keys/test_key", s, t)

    client.Set("test_key", "modified_test_val", 0)

    response := getRequestBody("/keys/test_key", s, t)
    assert.Equal(t, "test_val", string(response), "wrong value pulled from lru cache")
}

func TestLRUCacheExpiration(t *testing.T) {
    p := proxy.New(testRedis, testCapacity, testExpiration, testMaxClients)
    s := proxy.NewServer(testPort, p)

    client := getRedisClient(t)
    client.Set("expir_test_key", "expir_test_val", 0)

    getRequestBody("/keys/expir_test_key", s, t)

    client.Set("expir_test_key", "updated_expir_test_val", 0)

    time.Sleep(200 * time.Millisecond)
    response := getRequestBody("/keys/expir_test_key", s, t)

    assert.NotEqual(t, "expir_test_val", string(response), "lru cache did not invalidate expired value")
    assert.Equal(t, "updated_expir_test_val", string(response), "proxy didn't get updated value from redis")
}

func TestLRUCacheEviction(t *testing.T) {
    p := proxy.New(testRedis, testCapacity, testExpiration, testMaxClients)
    s := proxy.NewServer(testPort, p)

    client := getRedisClient(t)

    // load redis to lruCacheCapacity
    for i := 0; i < testCapacity + 1; i++ {
        key := fmt.Sprintf("test_key_%d", i)
        value := fmt.Sprintf("test_val_%d", i)
        client.Set(key, value, 0)
    }

    // query proxy server for all the redis values just created
    for i := 0; i < testCapacity + 1; i++ {
        keyPath := fmt.Sprintf("/keys/test_key_%d", i)
        getRequestBody(keyPath, s, t)
    }

    // update all old redis values
    for i := 0; i < testCapacity + 1; i++ {
        key := fmt.Sprintf("test_key_%d", i)
        value := fmt.Sprintf("test_val_updated_%d", i)
        client.Set(key, value, 0)
    }

    evictedKeyPath := fmt.Sprintf("/keys/test_key_%d", 0)
    response := getRequestBody(evictedKeyPath, s, t)

    assert.NotEqual(t, "test_val_0", string(response), "Cache didn't remove entry after reaching lruCacheCapacity")
    assert.Equal(t, "test_val_updated_0", string(response), "Proxy server did not query redis for evicted entry")
}