package redis

import (
    "fmt"
    "github.com/go-redis/redis"
)


type Store struct {
    Conn *redis.Client
}


type NotFoundError struct {
    Key string
}

func (e *NotFoundError) Error() string {
    return fmt.Sprintf("key not found %s", e.Key)
}


func New(addr string) *Store {
    client := redis.NewClient(&redis.Options{
        Addr: addr,
    })

    return &Store{
        Conn: client,
    }
}

func (r *Store) Get(key string) (string, error) {
    value, err := r.Conn.Get(key).Result()

    if err != nil {
        if err == redis.Nil {
            return "", &NotFoundError{key}
        }

        return "", err
    }

    return value, nil
}

