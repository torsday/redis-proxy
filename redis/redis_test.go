package redis_test

import (
    "testing"
    "github.com/stretchr/testify/assert"

    "red-prox/redis"
)

func TestGetKey(t *testing.T) {
    r := redis.New("redis:6379")
    r.Conn.Set("test_key", "test_val", 0)

    returnedVal, _ := r.Get("test_key")

    assert.Equal(t, "test_val", returnedVal, "incorrect value retrieved from redis")
}