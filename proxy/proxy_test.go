package proxy_test

import (
    "net"
    "net/http"
    "testing"
    "github.com/stretchr/testify/assert"

    "red-prox/proxy"
)

func TestProxyServer(t *testing.T) {
    p := proxy.New("redis:6379", 10, 100, 10)
    s := proxy.NewServer(8080, p)

    l, _ := net.Listen("tcp", ":0")

    defer l.Close()
    go s.Serve(l) 
    
    res, _ := http.Get("http://" + l.Addr().String() + "/")

    assert.Equal(t, http.StatusOK, res.StatusCode, "proxy returned non-200 status")
}