package proxy 

import (
    "fmt"
    "net/http"
    "strconv"

    "github.com/gorilla/mux"

    "red-prox/redis"
    "red-prox/lrucache"
)

type Proxy struct {
    redisDb            *redis.Store
    lruCache           *lrucache.CacheStore
    maxNumberOfClients int
    semaphore          chan struct{} // limit number of clients (https://pauladamsmith.com/blog/2016/04/max-clients-go-net-http.html)
}

func New(redisAddress string, lruCacheCapacity int, globalExpiration int, maxClients int) *Proxy {
    r := redis.New(redisAddress)
    c := lrucache.New(lruCacheCapacity, globalExpiration)

    return &Proxy{
        redisDb:            r,
        lruCache:           c,
        maxNumberOfClients: maxClients,
        semaphore:          make(chan struct{}, maxClients),
    }
}

func NewServer(port int, p *Proxy) *http.Server{
    router := mux.NewRouter()
    router.HandleFunc("/", p.IndexHandler).Methods("GET")
    router.HandleFunc("/keys/{key}", p.GetHandler)

    server := &http.Server{
        Addr: ":" + strconv.Itoa(port),
        Handler: router,
    }

    return server
}

func (prox *Proxy) IndexHandler(w http.ResponseWriter, r *http.Request) {
   w.WriteHeader(http.StatusOK)
}

func (prox *Proxy) GetHandler(w http.ResponseWriter, r *http.Request) {
    prox.semaphore <- struct{}{}
    defer func() { <-prox.semaphore }()
    
    defer r.Body.Close()

    vars := mux.Vars(r)
    key, _ := vars["key"]

    // First check LRU cache for entry
    if value, ok := prox.lruCache.Get(key); ok {
        w.WriteHeader(http.StatusOK)
        w.Write([]byte(value))
        return
    }

    // Then check redis for entry
    value, err := prox.redisDb.Get(key)
    if err != nil {
        if keyError, ok := err.(*redis.NotFoundError); ok {
            w.WriteHeader(http.StatusNotFound)
            w.Write([]byte("404: Key not found: " + keyError.Key))
            return 
        }
        
        fmt.Println(err)
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte("500: Internal Service Error"))
        return   
    }

    // Update LRU cache with redis find.
    prox.lruCache.Set(key, value)
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(value))
    return    
}
