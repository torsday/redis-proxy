package lrucache_test

import (
    "fmt"
    "time"
    "testing"

    "github.com/stretchr/testify/assert"

    "red-prox/lrucache"
)

var (
    capacity = 10
    globalExpiration = 100
)

func TestExpiration(t *testing.T) {
    c := lrucache.New(capacity, globalExpiration)
    c.Set("test_key", "test_val")

    time.Sleep(101 * time.Millisecond)
    value, _ := c.Get("test_key")

    assert.Equal(t, "", value, "returned non-empty value for expired key")
}

func TestEviction(t *testing.T) {
    c := lrucache.New(capacity, globalExpiration)
    doomedKey := "doomed_key"

    c.Set(doomedKey, "lorem ipsum") // this key should be pushed out by the subsequent calls

    for i := 0; i < capacity; i++ {
        key := fmt.Sprintf("%s%d", "test_key", i)
        c.Set(key, "lorem ipsum")
    }
    for i := 0; i < capacity; i++ {
        key := fmt.Sprintf("%s%d", "test_key", i)
        c.Get(key)
    }

    value, isFound := c.Get(doomedKey)
    assert.Equal(t, "", value, "cache returned value for expired key")
    assert.Equal(t, false, isFound, "expired key found")
}