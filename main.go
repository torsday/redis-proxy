package main 

import (
    "flag"
    "fmt"

    "red-prox/proxy"
)

var port = flag.Int("port", 8080, "Port for address")
var lruCacheCapacity = flag.Int("lruCacheCapacity", 100, "LRU Cache lruCacheCapacity")
var globalExpiration = flag.Int("global-expiration", 60000, "LRU Cache expiration (ms)")
var maxClients = flag.Int("max-clients", 5, "Max number of clients")
var redisAddressAndPort = flag.String("redis-address-and-port", "redis:6379", "Redis address and port")

func main() {
    flag.Parse()

    // Initialize Proxy
    p := proxy.New(*redisAddressAndPort, *lruCacheCapacity, *globalExpiration, *maxClients)

    // Initialize server
    server := proxy.NewServer(*port, p)

    // Start Server
    server.ListenAndServe()

    fmt.Println("Server running")
}   
