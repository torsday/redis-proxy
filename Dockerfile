FROM golang:1.12.1

ARG app_env
ENV APP_ENV $app_env

ENV LRU_CACHE_CAPACITY=1000
ENV GLOBAL_EXPIRATION=60000
ENV PORT=8080
ENV REDIS_ADDRESS_AND_PORT=redis:6379
ENV MAX_CLIENTS=5

RUN go get -u github.com/pilu/fresh \
    && go get -u github.com/golang/groupcache/lru \
    && go get -u github.com/gorilla/mux \
    && go get -u github.com/go-redis/redis \
    && go get -u github.com/stretchr/testify/assert

COPY . /go/src/red-prox
WORKDIR /go/src/red-prox

RUN go build
CMD ./redis-proxy -capacity 1000 -global-expiration 8000 -port 8080 -max-clients 5

EXPOSE 8080

