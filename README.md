# A GoLang Redis Proxy w/ LRU Cache

A proxy server (with cache) wrapping a redis instance. Access to the server is accomplished through an HTTP `GET` request. 

As this is a test app, the setting of redis values is not accomplished via API, rather the tests build the data stores directly.

---

## Architecture

I've split the main components of the app in to three domain spaces: The proxy, the LRU Cache, and the Redis data source. A semaphore is used to limit those with concurrent access to the proxy; beyond that, clients queue up.

### API

- `GET /keys/{key_to_query}`

### Data Flow

A client (up to a determined amount), connects to the proxy, which they query with an HTTP GET method call. The proxy checks its LRU Cache for the value first. If it finds it there, it returns the value to the client. If it is found, it first updates the LRU Cache, then returns the value. If not found, it returns the relevant 404 or 500, with no update to cache.

### Algorithm Complexity

Both lookups for redis and the LRU cache, while in memory, are O(1) time.

---

## Time Spent & Conclusion

As I'm new to GoLang, the lion's share of my time was spent diving into that language, determining the appropriate libraries, and how to implement them. The architecture itself took about 10 minutes to come up with, and 50 more minutes to ponder over.

I did not get to the reach questions, and would have liked to have improved the depth of the README, but I ran out of time.

---

### Setup

#### Prerequisites

- make
- docker, docker-compose
- bash

#### Installation

```sh
# clone the repo
git clone git@gitlab.com:torsday/redis-proxy.git
```

#### Configuration

Configuration is accomplished via the `.env` file

```
GLOBAL_EXPIRATION=10000
LRU_CACHE_CAPACITY=30
MAX_CLIENTS=7
PORT=8080
REDIS_ADDRESS_AND_PORT=redis:6379
```

#### Building & Running


```sh
# cd into the repo
cd redis-proxy
```

```sh
# build docker container
make build
```

```sh
# run in docker container
make run
```

```sh
# run tests
make test
```

```sh
# stop container
make stop
```
